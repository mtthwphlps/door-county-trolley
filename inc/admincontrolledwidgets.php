<?php
function foundaframework_register_sidebars() {
	register_sidebar(
		array(
			'id' => 'defaultsidebar',
			'name' => __( 'Sidebar-Right' ),
			'description' => __( 'The sidebar on the right of most pages.' ),
			'before_widget' => '<div id="%1$s" class="defaultsidebarwidget widget %2$s"><div class="defaultsidebarwidgetwrapper">',
			'after_widget' => '</div></div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
}
add_action( 'widgets_init', 'foundaframework_register_sidebars' );
?>