<?php
function is_child($pageID) { 
	global $post; 
	if( is_page() && ($post->post_parent==$pageID) ) {
               return true;
	} else { 
               return false; 
	}
}

add_theme_support( 'post-thumbnails' ); 

function custom_login_message() {
	$message = "<div id='fvwd-wp-login'><strong>Customer Support</strong><br />
<a href='http://foxvalleywebdesign.com' target='_blank'>www.foxvalleywebdesign.com</a><br />
Toll Free: (920) 434-9772</div>";
	return $message;
}
add_filter('login_message', 'custom_login_message');

// Change the text in the admin footer
function wpse_edit_footer()
{
    add_filter( 'admin_footer_text', 'wpse_edit_text', 11 );
}
function wpse_edit_text($content) {
    return "Website by: <a href='http://foxvalleywebdesign.com' terget='_blank'>Fox Valley Web Design <sup>TM</sup></a>";
}
add_action( 'admin_init', 'wpse_edit_footer' );
add_action( 'login_enqueue_scripts', 'my_login_logo' );
add_filter('login_headertitle', 'change_title_on_logo');

function loginpage_custom_link() {
	return '/';
}
add_filter('login_headerurl','loginpage_custom_link');


/**/
function show_social_icons() {
	$themesettings = get_option( 'custom_theme_options_social' );
	if($themesettings['socialenabled'] == 1) {
		$return = '';
		$initialdata = false;
		$return .= "<div id=\"socialicons\">";
		$socialsites = array("behance","blogger","delicious","designfloat","deviantart","digg","facebook","flickr","lastfm","linkedin","livejournal","megavideo","myspace","piano","playstation","reddit","rss","socialvibe","spotify","stumbleupon","technorati","tumblr","twitpic","twitter","vimeo","wordpress","youtube");
		foreach($socialsites as $socialsite) {
			if($themesettings[$socialsite] != '' && $themesettings[$socialsite] != 'disabled') {
				if($socialsite == 'rss') {
					$return .= "<a class=\"socialicon rss\" href=\"".get_bloginfo('rss2_url')."\" target=\"_blank\"><img src=\"".get_bloginfo( 'template_directory' )."/img/icons/32/rss.png\" alt=\"RSS\" /></a>";
				} else {
					$return .= "<a class=\"socialicon ".$socialsite."\" href=\"".$themesettings[$socialsite]."\" target=\"_blank\"><img src=\"".get_bloginfo( 'template_directory' )."/img/icons/32/".$socialsite.".png\" alt=\"".$socialsite."\" /></a>";
					if($themesettings[$socialsite] == '#') {
						$initialdata = true;
					}
				}
			}
		}
		$return .= "</div>";
		if($initialdata == true) {
			$return .= '<div id="socialiconsnotice" style="font-size: small; color: red;">Some or all of the links above are not set</div>';
		}
		return stripslashes($return);
	}
}

add_shortcode('show_socialicons', 'show_social_icons');

add_filter('widget_text', 'do_shortcode');

function templatedir() {
	return get_template_directory_uri();
}
add_shortcode('templatedir', 'templatedir');
?>