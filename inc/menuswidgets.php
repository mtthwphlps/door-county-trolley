<?php
function register_foundaframework_menus() {
	register_nav_menus(
		array( 'homeleftmenu' => 'Home - Left Menu', 'homerightmenu' => 'Home - Right Menu', 'insidemenu' => 'Inside Menu' )
	);
}
add_action( 'init', 'register_foundaframework_menus' );

function foundaframework_home_left_menu() {
	wp_nav_menu(array(
    	'theme_location' => 'homeleftmenu',// where it's located in the theme
    	'menu' => 'Home - Left Menu',// nav name
    	'container_id' => 'homeleftmenu',// class of container (should you choose to use it)
        'depth' => 1,// limit the depth of the nav
	));
}
function foundaframework_home_right_menu() {
	wp_nav_menu(array(
    	'theme_location' => 'homerightmenu',
    	'menu' => 'Home - Right Menu',
    	'container_id' => 'homerightmenu',// class of container (should you choose to use it)
        'depth' => 1,
	));
}
function foundaframework_inside_menu() {
	wp_nav_menu(array(
    	'theme_location' => 'insidemenu',
    	'menu' => 'Inside Menu',
    	'container_id' => 'insidemenu',// class of container (should you choose to use it)
        'depth' => 1,
	));
}

//require 'pagenavtitle-meta-box.php';

?>