<?php
/* CUSTOMIZE THIS!!!*/
function my_login_logo() { ?>
    <style type="text/css">
		body.login {
			background: #DCDCDC url(<?php echo get_bloginfo( 'template_directory' ) ?>/img/background.jpg) top center no-repeat;
			background-size: 100% auto;
			background-attachment: fixed;
		}
		#login {
			padding: 10px 0 0 0;
		}
        body.login div#login h1 a {
            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/img/logo.png);
			background-size: 373px 206px;
			width: 373px;
			height: 206px;
			margin: 0 0 0 -29px;
        }
		.login #nav a, .login #backtoblog a {
			text-shadow: 0 0 5px #ffffff;
			color: #000000;
		}
		#fvwd-wp-login {
			background: none repeat scroll 0 0 #FFFFFF;
			border: 1px solid #E5E5E5;
			box-shadow: 0;
			font-weight: normal;
			margin: 0 0 0 0;
			padding: 10px;
			border-radius: 0;
			position: absolute;
			top: 605px;
			width: 298px;
			text-align: center;
		}
    </style>
<?php }

function change_title_on_logo() {
	return 'Door County Trolley';
}

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'featured-image', 973, 171, true );
}
/* STOP */
?>