<?php
/*
single posts
*/
$currentpagename = 'single';
?>
<?php require 'header.php'; ?>
<?php require 'header-inside.php'; ?>
<div id="contentcontainer">
	<div id="content">
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); ?>
				<h1><?php the_title();?></h1>
				<?php the_content();
			endwhile;
		endif; ?>
	</div>
</div>
<?php require 'footer-inside.php'; ?>
<?php require 'footer.php'; ?>