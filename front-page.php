<?php
/*
Front page
*/
$currentpagename = 'front-page';
?>
<?php require 'header.php'; ?>
<div id="home-widecontainer">
	<div id="home-bigcontainer">
		<!-- HEADER -->
		<div id="home-header">
			<div id="home-headerlogo">
				<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/home-logo.png" alt="Door County Trolley" /></a>
			</div>
		</div>
		<!-- END HEADER -->
		<!-- PAGE BODY -->
		<div id="home-middle">
			<div id="home-bannercontainer">
				<?php if(function_exists('meteor_slideshow')) {
						meteor_slideshow("homebanner", "");
				} ?>
			</div>
			<div id="home-middlecontainer">
				<div id="home-navleft">
					<div class="home-nav">
						<?php foundaframework_home_left_menu(); ?>
					</div>
				</div>
				<div id="home-navright">
					<div class="home-nav">
						<?php foundaframework_home_right_menu(); ?>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE BODY -->
		<!-- PAGE FOOTER -->
		<div id="home-footer">
			<div class="home-footerlink left">
				<a href="http://www.doorcountytrolley.com/wp-content/uploads/2014/05/DCTrolley_brochure.pdf" target="new">Our Brochure</a>
			</div>
			<div class="home-footerlink center">
				<a href="http://www.zerve.com/DoorTrolley" target="new">Buy Tickets <img src="<?php echo get_template_directory_uri(); ?>/img/zervelogo.jpg" alt="Zerve" /></a>
			</div>
			<div class="home-footerlink right">
				<a href="http://www.facebook.com/pages/Door-County-Trolley-Inc/107278709314303" target="new">Facebook</a>
			</div>
		</div>
		<div id="home-footerbottom">
			<div id="home-footerrow" class="footer-social">
				<?php echo show_social_icons(); ?>
			</div>
			<div id="home-footerrow" class="footer-copy">
				Door County Trolley * Sturgeon Bay, Wisconsin<br />
				Scenic Tours Of Door County Wisconsin<br />
				All Rights Reserved
			</div>
			<div id="home-footerrow" class="footer-tourinfo">
				For Tour Tickets &amp; Information, please contact Zerve at 1-866-604-5573 or Click Here
			</div>
			<div id="home-footerrow" class="footer-private">
				To Inquire About Privite Charters or Wedding Rentals call 1-920-868-1100 or Fill Out Our Online Form
			</div>
			<div id="home-footerrow" class="footer-credit">
				Site by: <a href="http://foxvalleywebdesign.com" target="_blank">Fox Valley Web Design <sup>TM</sup></a> <button id="soundonoff" type="button" onclick="toggleSound();" alt="toggle sound"></button>
			</div>
		</div>
	</div>
</div>
<div id="navsounds">
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	// Add the audio elements on load
	jQuery('.home-nav div ul').children().each(function(index) {
		var elemID = jQuery(this).attr("id");
		var HTMLToAdd = '<audio id="trollybell-' + elemID + '"><source src="<?php echo get_template_directory_uri(); ?>/audio/rollover-trolleybell.mp3"></source><source src="<?php echo get_template_directory_uri(); ?>/audio/rollover-trolleybell.ogg"></source></audio>';
		jQuery("#navsounds").append(HTMLToAdd);
		console.log("Audio element added for " + elemID);
	});
	// Trigger the correct audio element on mouseover
	jQuery(".home-nav a").mouseenter(function() {
		var elemID = jQuery(this).parent().attr("id");
		var audio = jQuery("#navsounds #trollybell-" + elemID)[0];
		console.log("Nav rollover on link " + elemID + ", play sound");
		audio.play();
	});
});
</script>
<script type="text/javascript">
	function toggleSound() {
		var audio = jQuery("#home-background-sound")[0];
		if (audio.paused == false) {
			audio.pause();
			jQuery("#soundonoff").css("background-position", "-15px 0px");
		} else {
			audio.play();
			jQuery("#soundonoff").css("background-position", "0px 0px");
		}
	}
</script>
<audio loop autoplay id="home-background-sound">
	<source src="<?php echo get_template_directory_uri(); ?>/audio/summer-seagull.mp3" type="audio/mpeg">
	<source src="<?php echo get_template_directory_uri(); ?>/audio/summer-seagull.ogg" type="audio/ogg">
</audio>
<?php require 'footer.php'; ?>