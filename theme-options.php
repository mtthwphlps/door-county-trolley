<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	register_setting( 'custom_options_social', 'custom_theme_options_social', 'theme_options_validate' );
	register_setting( 'custom_options_analytics', 'custom_theme_options_analytics', 'theme_options_validate' );
	register_setting( 'custom_options_menus', 'custom_theme_options_menus', 'theme_options_validate' );
	register_setting( 'custom_options_widgets', 'custom_theme_options_widgets', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_theme_page( __( 'Social Media', 'frameworkoptions' ), __( 'Social Media', 'frameworkoptions' ), 'edit_theme_options', 'theme_options_social', 'theme_options_do_page_social' );
	//add_theme_page( __( 'Google Analytics', 'frameworkoptions' ), __( 'Google Analytics', 'frameworkoptions' ), 'edit_theme_options', 'theme_options_analytics', 'theme_options_do_page_analytics' );
	//add_theme_page( __( 'Register Menus', 'frameworkoptions' ), __( 'Register Menus', 'frameworkoptions' ), 'edit_theme_options', 'theme_options_menus', 'theme_options_do_page_menus' );
	//add_theme_page( __( 'Register Widgets', 'frameworkoptions' ), __( 'Register Widgets', 'frameworkoptions' ), 'edit_theme_options', 'theme_options_widgets', 'theme_options_do_page_widgets' );
}

/**
 * Create arrays for our select and radio options
 */
$select_options = array(
	'disabled' => array(
		'value' =>	'disabled',
		'label' => __( 'Disabled', 'frameworkoption' )
	),
	'enabled' => array(
		'value' =>	'enabled',
		'label' => __( 'Enabled', 'frameworkoption' )
	),
);

/**
 * Create the options page
 */
 
function theme_options_do_page_social() {
	global $select_options;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>Social Media Settings</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Options saved', 'frameworkoption' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'custom_options_social' ); ?>
			<?php $options = get_option( 'custom_theme_options_social' ); ?>
			<p><input id="custom_theme_options_social[socialenabled]" name="custom_theme_options_social[socialenabled]" type="checkbox" value="1" <?php checked( '1', $options['socialenabled'] ); ?> /> <strong>Enable Social Media Icons</strong></p>
			<p><em><strong>NOTE</strong>: blank = disabled</em></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/behance.png" alt="Behance" /> <strong>Behance</strong> (full page URL)<br />
			<input id="custom_theme_options_social[behance]" class="regular-text" type="text" name="custom_theme_options_social[behance]" value="<?php esc_attr_e( $options['behance'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/blogger.png" alt="Blogger" /> <strong>Blogger</strong> (full page URL)<br />
			<input id="custom_theme_options_social[blogger]" class="regular-text" type="text" name="custom_theme_options_social[blogger]" value="<?php esc_attr_e( $options['blogger'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/delicious.png" alt="Delicious" /> <strong>Delicious</strong> (full page URL)<br />
			<input id="custom_theme_options_social[delicious]" class="regular-text" type="text" name="custom_theme_options_social[delicious]" value="<?php esc_attr_e( $options['delicious'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/designfloat.png" alt="Design Float" /> <strong>Design Float</strong> (full page URL)<br />
			<input id="custom_theme_options_social[designfloat]" class="regular-text" type="text" name="custom_theme_options_social[designfloat]" value="<?php esc_attr_e( $options['designfloat'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/deviantart.png" alt="deviantART" /> <strong>deviantART</strong> (full page URL)<br />
			<input id="custom_theme_options_social[deviantart]" class="regular-text" type="text" name="custom_theme_options_social[deviantart]" value="<?php esc_attr_e( $options['deviantart'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/digg.png" alt="Digg" /> <strong>Digg</strong> (full page URL)<br />
			<input id="custom_theme_options_social[v]" class="regular-text" type="text" name="custom_theme_options_social[digg]" value="<?php esc_attr_e( $options['digg'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/facebook.png" alt="Facebook" /> <strong>Facebook</strong> (full page URL)<br />
			<input id="custom_theme_options_social[facebook]" class="regular-text" type="text" name="custom_theme_options_social[facebook]" value="<?php esc_attr_e( $options['facebook'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/flickr.png" alt="Flickr" /> <strong>Flickr</strong> (full page URL)<br />
			<input id="custom_theme_options_social[flickr]" class="regular-text" type="text" name="custom_theme_options_social[flickr]" value="<?php esc_attr_e( $options['flickr'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/lastfm.png" alt="LastFM" /> <strong>LastFM</strong> (full page URL)<br />
			<input id="custom_theme_options_social[lastfm]" class="regular-text" type="text" name="custom_theme_options_social[lastfm]" value="<?php esc_attr_e( $options['lastfm'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/linkedin.png" alt="LinkedIn" /> <strong>LinkedIn</strong> (full page URL)<br />
			<input id="custom_theme_options_social[linkedin]" class="regular-text" type="text" name="custom_theme_options_social[linkedin]" value="<?php esc_attr_e( $options['linkedin'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/livejournal.png" alt="LiveJournal" /> <strong>LiveJournal</strong> (full page URL)<br />
			<input id="custom_theme_options_social[livejournal]" class="regular-text" type="text" name="custom_theme_options_social[livejournal]" value="<?php esc_attr_e( $options['livejournal'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/megavideo.png" alt="Megavideo" /> <strong>Megavideo</strong> (full page URL)<br />
			<input id="custom_theme_options_social[megavideo]" class="regular-text" type="text" name="custom_theme_options_social[megavideo]" value="<?php esc_attr_e( $options['megavideo'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/myspace.png" alt="Myspace" /> <strong>Myspace</strong> (full page URL)<br />
			<input id="custom_theme_options_social[myspace]" class="regular-text" type="text" name="custom_theme_options_social[myspace]" value="<?php esc_attr_e( $options['myspace'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/piano.png" alt="Piano" /> <strong>Piano</strong> (full page URL)<br />
			<input id="custom_theme_options_social[piano]" class="regular-text" type="text" name="custom_theme_options_social[piano]" value="<?php esc_attr_e( $options['piano'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/playstation.png" alt="Playstation" /> <strong>Playstation</strong> (full page URL)<br />
			<input id="custom_theme_options_social[playstation]" class="regular-text" type="text" name="custom_theme_options_social[playstation]" value="<?php esc_attr_e( $options['playstation'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/reddit.png" alt="Reddit" /> <strong>Reddit</strong> (full page URL)<br />
			<input id="custom_theme_options_social[reddit]" class="regular-text" type="text" name="custom_theme_options_social[reddit]" value="<?php esc_attr_e( $options['reddit'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/rss.png" alt="RSS" /> <strong>RSS</strong> (full page URL)<br />
			<em>The RSS URL will be determined automatically.</em><br />
			<select name="custom_theme_options_social[rss]">
				<?php
					$selected = $options['rss'];
					$p = '';
					$r = '';
					foreach ( $select_options as $option ) {
						$label = $option['label'];
						if ( $selected == $option['value'] ) // Make default first in list
							$p = "\n\t<option style=\"padding-right: 10px;\" selected='selected' value='" . esc_attr( $option['value'] ) . "'>$label</option>";
						else
							$r .= "\n\t<option style=\"padding-right: 10px;\" value='" . esc_attr( $option['value'] ) . "'>$label</option>";
						}
					echo $p . $r;
				?>
			</select>
			</p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/socialvibe.png" alt="Socialvibe" /> <strong>Socialvibe</strong> (full page URL)<br />
			<input id="custom_theme_options_social[socialvibe]" class="regular-text" type="text" name="custom_theme_options_social[socialvibe]" value="<?php esc_attr_e( $options['socialvibe'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/spotify.png" alt="Spotify" /> <strong>Spotify</strong> (full page URL)<br />
			<input id="custom_theme_options_social[spotify]" class="regular-text" type="text" name="custom_theme_options_social[spotify]" value="<?php esc_attr_e( $options['spotify'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/stumbleupon.png" alt="Stumbleupon" /> <strong>Stumbleupon</strong> (full page URL)<br />
			<input id="custom_theme_options_social[stumbleupon]" class="regular-text" type="text" name="custom_theme_options_social[stumbleupon]" value="<?php esc_attr_e( $options['stumbleupon'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/technorati.png" alt="Technorati" /> <strong>Technorati</strong> (full page URL)<br />
			<input id="custom_theme_options_social[technorati]" class="regular-text" type="text" name="custom_theme_options_social[technorati]" value="<?php esc_attr_e( $options['technorati'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/tumblr.png" alt="Tumblr" /> <strong>Tumblr</strong> (full page URL)<br />
			<input id="custom_theme_options_social[tumblr]" class="regular-text" type="text" name="custom_theme_options_social[tumblr]" value="<?php esc_attr_e( $options['tumblr'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/twitpic.png" alt="Twitpic" /> <strong>Twitpic</strong> (full page URL)<br />
			<input id="custom_theme_options_social[twitpic]" class="regular-text" type="text" name="custom_theme_options_social[twitpic]" value="<?php esc_attr_e( $options['twitpic'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/twitter.png" alt="Twitter" /> <strong>Twitter</strong> (full page URL)<br />
			<input id="custom_theme_options_social[twitter]" class="regular-text" type="text" name="custom_theme_options_social[twitter]" value="<?php esc_attr_e( $options['twitter'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/vimeo.png" alt="Vimeo" /> <strong>Vimeo</strong> (full page URL)<br />
			<input id="custom_theme_options_social[vimeo]" class="regular-text" type="text" name="custom_theme_options_social[vimeo]" value="<?php esc_attr_e( $options['vimeo'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/wordpress.png" alt="Wordpress" /> <strong>Wordpress</strong> (full page URL)<br />
			<input id="custom_theme_options_social[wordpress]" class="regular-text" type="text" name="custom_theme_options_social[wordpress]" value="<?php esc_attr_e( $options['wordpress'] ); ?>" /></p>
			<p><img src="<?php bloginfo( 'template_directory' ) ?>/img/icons/16/youtube.png" alt="YouTube" /> <strong>YouTube</strong> (full page URL)<br />
			<input id="custom_theme_options_social[youtube]" class="regular-text" type="text" name="custom_theme_options_social[youtube]" value="<?php esc_attr_e( $options['youtube'] ); ?>" /></p>
			<br />
			<p class="submit">
				<input name="custom_theme_options_social[form]" type="hidden" value="social" />
				<input type="submit" class="button-primary" value="<?php _e( 'Save', 'frameworkoption' ); ?>" />
			</p>
		</form>
	</div>
	<?php
}

function theme_options_do_page_analytics() {
	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;
	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Analytics Settings', 'frameworkoption' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Options saved', 'frameworkoption' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'custom_options_analytics' ); ?>
			<?php $options = get_option( 'custom_theme_options_analytics' ); ?>
			<p><strong>Enter the Google Analytics site ID below. (ie: UA-XXXXXXXX-X)</strong></p>
			<p><input id="custom_theme_options_analytics[analyticscode]" class="regular-text" type="text" name="custom_theme_options_analytics[analyticscode]" value="<?php echo esc_textarea( $options['analyticscode'] ); ?>" /></p>
			<p class="submit">
				<input name="custom_theme_options_analytics[form]" type="hidden" value="analytics" />
				<input type="submit" class="button-primary" value="<?php _e( 'Save', 'frameworkoption' ); ?>" />
			</p>
		</form>
	</div>
	<?php
}

function theme_options_do_page_menus() {
	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;
	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . ' Register Menus' . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Settings saved', 'frameworkoption' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'custom_options_menus' ); ?>
			<?php $options = get_option( 'custom_theme_options_menus' );?>
			<div id="menuscontainer">
			<?php if($options != '') {
				$menutotal = esc_textarea($options['menucount']);
				$menucurrent = 1;
				while($menucurrent <= $menutotal) { ?>
					<p>
					Name: <input id="custom_theme_options_menus[<?php echo $menucurrent; ?>][name]" class="regular-text" type="text" name="custom_theme_options_menus[<?php echo $menucurrent; ?>][name]" value="<?php echo esc_textarea( $options[$menucurrent]['name'] ); ?>" /> <em>ie: Main Menu</em><br />
					Slug: <input id="custom_theme_options_menus[<?php echo $menucurrent; ?>][slug]" class="regular-text" type="text" name="custom_theme_options_menus[<?php echo $menucurrent; ?>][slug]" value="<?php echo esc_textarea( $options[$menucurrent]['slug'] ); ?>" /> <em>no spaces, ie: mainmenu</em>
					</p>
				<?php $menucurrent = $menucurrent + 1;
				} ?>
				<input name="custom_theme_options_menus[menucount]" type="hidden" id="custom_theme_options_menus[menucount]" class="menucount" value="<?php echo $menutotal; ?>" />
			<?php } else { ?>
				<input name="custom_theme_options_menus[menucount]" type="hidden" id="custom_theme_options_menus[menucount]" class="menucount" value="1" /> <em>no spaces, ie: mainmenu</em>
				<p>
					Name: <input id="custom_theme_options_menus[1][name]" class="regular-text" type="text" name="custom_theme_options_menus[1][name]" value="" /> <em>ie: Main Menu</em><br />
					Slug: <input id="custom_theme_options_menus[1][slug]" class="regular-text" type="text" name="custom_theme_options_menus[1][slug]" value="" />
				</p>
			<?php } ?>
				</div>
				<input name="Add menu" type="button" value="Add Another Menu" onclick="addMenu();" />
			<p class="submit">
				<input name="custom_theme_options_menus[form]" type="hidden" value="menus" />
				<input type="submit" class="button-primary" value="Save" />
			</p>
			<script type="text/javascript">
				function addMenu() {
					menuCount = parseInt(jQuery(".menucount").val());
					newMenuCount = menuCount + 1;
					addThisCode = '<p>Name: <input id="custom_theme_options_menus[' + newMenuCount + '][name]" class="regular-text" type="text" name="custom_theme_options_menus[' + newMenuCount + '][name]" value="" /> <em>ie: Main Menu</em><br />Slug: <input id="custom_theme_options_menus[' + newMenuCount + '][slug]" class="regular-text" type="text" name="custom_theme_options_menus[' + newMenuCount + '][slug]" value="" /> <em>no spaces, ie: mainmenu</em></p>';
					jQuery("#menuscontainer").append(addThisCode);
					jQuery(".menucount").val(newMenuCount);
				}
			</script>
		</form>
	</div>
	<?php
}

function theme_options_do_page_widgets() {
	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;
	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . ' Register Widget Areas' . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong><?php _e( 'Settings saved', 'frameworkoption' ); ?></strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'custom_options_widgets' ); ?>
			<?php $options = get_option( 'custom_theme_options_widgets' );?>
			<div id="widgetscontainer">
			<?php if($options != '') {
				$widgettotal = esc_textarea($options['widgetcount']);
				$widgetcurrent = 1;
				while($widgetcurrent <= $widgettotal) { ?>
					<p>
					ID: <input id="custom_theme_options_widgets[<?php echo $widgetcurrent; ?>][id]" class="regular-text" type="text" name="custom_theme_options_widgets[<?php echo $widgetcurrent; ?>][id]" value="<?php echo esc_textarea( $options[$widgetcurrent]['id'] ); ?>" /> <em>no spaces, ie: defaultsidebar</em><br />
					Name: <input id="custom_theme_options_widgets[<?php echo $widgetcurrent; ?>][name]" class="regular-text" type="text" name="custom_theme_options_widgets[<?php echo $widgetcurrent; ?>][name]" value="<?php echo esc_textarea( $options[$widgetcurrent]['name'] ); ?>" /> <em>ie: Default Sidebar</em><br />
					Description: <input id="custom_theme_options_widgets[<?php echo $widgetcurrent; ?>][desc]" class="regular-text" type="text" name="custom_theme_options_widgets[<?php echo $widgetcurrent; ?>][desc]" value="<?php echo esc_textarea( $options[$widgetcurrent]['desc'] ); ?>" />
					</p>
				<?php $widgetcurrent = $widgetcurrent + 1;
				} ?>
				<input name="custom_theme_options_widgets[widgetcount]" type="hidden" id="custom_theme_options_widgets[widgetcount]" class="widgetcount" value="<?php echo $widgettotal; ?>" />
			<?php } else { ?>
				<input name="custom_theme_options_widgets[widgetcount]" type="hidden" id="custom_theme_options_widgets[widgetcount]" class="widgetcount" value="1" />
				<p>
					ID: <input id="custom_theme_options_widgets[1][id]" class="regular-text" type="text" name="custom_theme_options_widgets[1][id]" value="" /> <em>no spaces, ie: defaultsidebar</em><br />
					Name: <input id="custom_theme_options_widgets[1][name]" class="regular-text" type="text" name="custom_theme_options_widgets[1][name]" value="" /> <em>ie: Default Sidebar</em>
					<br />
					Description: <input id="custom_theme_options_widgets[1][desc]" class="regular-text" type="text" name="custom_theme_options_widgets[1][desc]" value="" />
				</p>
			<?php } ?>
				</div>
				<input name="Add widget" type="button" value="Add Another Widget Area" onclick="addWidget();" />
			<p class="submit">
				<input name="custom_theme_options_widgets[form]" type="hidden" value="widgets" />
				<input type="submit" class="button-primary" value="Save" />
			</p>
			<script type="text/javascript">
				function addWidget() {
					widgetCount = parseInt(jQuery(".widgetcount").val());
					newWidgetCount = widgetCount + 1;
					addThisCode = '<p>ID: <input id="custom_theme_options_widgets[' + newWidgetCount + '][id]" class="regular-text" type="text" name="custom_theme_options_widgets[' + newWidgetCount + '][id]" value="" /> <em>no spaces, ie: defaultsidebar</em><br />Name: <input id="custom_theme_options_widgets[' + newWidgetCount + '][name]" class="regular-text" type="text" name="custom_theme_options_widgets[' + newWidgetCount + '][name]" value="" /> <em>ie: Default Sidebar</em><br />Description: <input id="custom_theme_options_widgets[' + newWidgetCount + '][desc]" class="regular-text" type="text" name="custom_theme_options_widgets[' + newWidgetCount + '][desc]" value="" /></p>';
					jQuery("#widgetscontainer").append(addThisCode);
					jQuery(".widgetcount").val(newWidgetCount);
				}
			</script>
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
	global $select_options;

	if($input['form'] == 'social') {
		// Enabled or disabled
		$input['socialenabled'] = wp_filter_nohtml_kses( $input['socialenabled'] );
		//Behance
		$input['behance'] = wp_filter_nohtml_kses( $input['behance'] );
		//Blogger
		$input['blogger'] = wp_filter_nohtml_kses( $input['blogger'] );
		//Delicious
		$input['delicious'] = wp_filter_nohtml_kses( $input['delicious'] );
		//Designfloat
		$input['designfloat'] = wp_filter_nohtml_kses( $input['designfloat'] );
		
		$input['deviantart'] = wp_filter_nohtml_kses( $input['deviantart'] );
		
		$input['digg'] = wp_filter_nohtml_kses( $input['digg'] );
		
		$input['facebook'] = wp_filter_nohtml_kses( $input['facebook'] );
		
		$input['flickr'] = wp_filter_nohtml_kses( $input['flickr'] );
		
		$input['lastfm'] = wp_filter_nohtml_kses( $input['lastfm'] );
		
		$input['linkedin'] = wp_filter_nohtml_kses( $input['linkedin'] );
		
		$input['livejournal'] = wp_filter_nohtml_kses( $input['livejournal'] );
		
		$input['megavideo'] = wp_filter_nohtml_kses( $input['megavideo'] );
		
		$input['myspace'] = wp_filter_nohtml_kses( $input['myspace'] );
		
		$input['piano'] = wp_filter_nohtml_kses( $input['piano'] );
		
		$input['playstation'] = wp_filter_nohtml_kses( $input['playstation'] );
		
		$input['reddit'] = wp_filter_nohtml_kses( $input['reddit'] );
		
		$input['rss'] = wp_filter_nohtml_kses( $input['rss'] );
		
		$input['socialvibe'] = wp_filter_nohtml_kses( $input['socialvibe'] );
		
		$input['spotify'] = wp_filter_nohtml_kses( $input['spotify'] );
		
		$input['stumbleupon'] = wp_filter_nohtml_kses( $input['stumbleupon'] );
		
		$input['technorati'] = wp_filter_nohtml_kses( $input['technorati'] );
		
		$input['tumblr'] = wp_filter_nohtml_kses( $input['tumblr'] );
		
		$input['twitpic'] = wp_filter_nohtml_kses( $input['twitpic'] );
		
		$input['twitter'] = wp_filter_nohtml_kses( $input['twitter'] );
		
		$input['vimeo'] = wp_filter_nohtml_kses( $input['vimeo'] );
		
		$input['wordpress'] = wp_filter_nohtml_kses( $input['wordpress'] );
		
		$input['youtube'] = wp_filter_nohtml_kses( $input['youtube'] );
	}
	if($input['form'] == 'analytics') {
		// Analytics
		$input['analyticscode'] = wp_filter_post_kses( $input['analyticscode'] );
	}
	if($input['form'] == 'menus') {
		$currentcount = 1;
		$totalcount = wp_filter_post_kses($input['menucount']);
		$input['menucount'] = wp_filter_post_kses($input['menucount']);
		$filedata = "<?php
function register_foundaframework_menus() {
	register_nav_menus(
		array( ";
		$seperator = '';
		while($currentcount <= $totalcount) {
			if($input[$currentcount]['name'] != '') {
				$menuname = wp_filter_post_kses($input[$currentcount]['name']);
				$input[$currentcount]['name'] = wp_filter_post_kses($input[$currentcount]['name']);
				$menuslug = wp_filter_post_kses($input[$currentcount]['slug']);
				$input[$currentcount]['slug'] = wp_filter_post_kses($input[$currentcount]['slug']);
				$filedata .= $seperator."'".$menuslug."' => '".$menuname."'";
				$seperator = ', ';
			}
			$currentcount = $currentcount + 1;
		}
		$filedata .= " )
	);
}
add_action( 'init', 'register_foundaframework_menus' );
?>";
		$fp = fopen(dirname(__FILE__).'/inc/admincontrolledmenus.php', 'w');
		fwrite($fp, $filedata);
		fclose($fp);
	}
		if($input['form'] == 'widgets') {
		$currentcount = 1;
		$totalcount = wp_filter_post_kses($input['widgetcount']);
		$input['widgetcount'] = wp_filter_post_kses($input['widgetcount']);
		$filedata = "<?php
function foundaframework_register_sidebars() {
	";
		$s = '$s';
		while($currentcount <= $totalcount) {
			if($input[$currentcount]['id'] != '') {
				$widgetid = wp_filter_post_kses($input[$currentcount]['id']);
				$input[$currentcount]['id'] = wp_filter_post_kses($input[$currentcount]['id']);
				$widgetname = wp_filter_post_kses($input[$currentcount]['name']);
				$input[$currentcount]['name'] = wp_filter_post_kses($input[$currentcount]['name']);
				$widgetdesc = wp_filter_post_kses($input[$currentcount]['desc']);
				$input[$currentcount]['desc'] = wp_filter_post_kses($input[$currentcount]['desc']);
				if($widgetid == 'defaultsidebar' || $widgetid == 'leftsidebar' || $widgetid == 'blogsidebar') {
					$displaynone = '';
				} else {
					$displaynone = ' style=\"display: none;\"';
				}
				$filedata .= "register_sidebar(
		array(
			'id' => '".$widgetid."',
			'name' => __( '".$widgetname."' ),
			'description' => __( '".$widgetdesc."' ),
			'before_widget' => '<div id=\"%1$s\" class=\"".$widgetid."widget widget %2$s\"><div class=\"".$widgetid."widgetwrapper\">',
			'after_widget' => '</div></div>',
			'before_title' => '<h3 class=\"widget-title\"".$displaynone.">',
			'after_title' => '</h3>'
		)
	);";
			}
			$currentcount = $currentcount + 1;
		}
		$filedata .= "
}
add_action( 'widgets_init', 'foundaframework_register_sidebars' );
?>";
		$fp = fopen(dirname(__FILE__).'/inc/admincontrolledwidgets.php', 'w');
		fwrite($fp, stripslashes($filedata));
		fclose($fp);
	}
	return $input;
}

// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/