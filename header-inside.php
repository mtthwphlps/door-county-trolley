<div id="inside-widecontainer">
	<div id="inside-bigcontainer">
		<div id="inside-header" class="<?php echo $foundaheadclass; ?>">
			<div id="inside-header-logo">
				<div id="header-logo-standard">
					<?php if($foundaheadclass == 'ghosttour') { ?>
						<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/inside-logo-ghost.png" alt="Door County Trolley" /></a>
					<?php } elseif($foundaheadclass == 'wintertour') { ?>
						<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/inside-logo-snow.png" alt="Door County Trolley" /></a>
					<?php } else { ?>
						<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/inside-logo.png" alt="Door County Trolley" /></a>
					<?php } ?>
				</div>
				<div id="header-logo-glow">
					<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/inside-logo-glow.png" alt="Door County Trolley" /></a>
				</div>
			</div>
			<?php if($foundaheadclass == 'ghosttour') { ?>
				<div id="ghost">
					<img src="<?php echo get_template_directory_uri(); ?>/img/ghost.png" />
				</div>
			<?php }
			if($foundaheadclass == 'wintertour') { ?>
				<div id="snowflakeContainer" style="width: 961px; height: 215px; color: #ffffff;">
				</div>
			<?php } ?>
		</div>
		<div id="inside-body">
			<div id="inside-sidebar">
				<div id="inside-leftnavtop">
				</div>
				<div id="inside-leftnav">
					<?php foundaframework_inside_menu(); ?>
				</div>
				<div id="inside-leftnavbottom">
				</div>
				<div id="inside-widgets">
				</div>
			</div>
			<div id="inside-content">
