<?php
/*
Template Name: Ghost Tours
Ghost tour template
*/
$currentpagename = 'page-ghost';
?>
<?php require 'header.php'; ?>
<?php $foundaheadclass = 'ghosttour'; ?>
<?php require 'header-inside.php'; ?>
<div id="contentcontainer">
	<div id="content">
		<?php if (have_posts()) {
			while (have_posts()) {
				the_post(); ?>
				<h1><?php the_title();?></h1>
				<?php the_content(); ?>
			<?php }
		} else { ?>
			<strong>Sorry, we couldn't find anything.</strong>
		<?php } ?>
	</div>
</div>
<style type="text/css">
.ghosttour #ghost {
    width: 961px;
    height: 210px;
	margin: 0 19px;
	overflow: hidden;
    position: absolute;
    top: 0;
	left: 0;
	z-index: 100;
}
.ghosttour #ghost img {
	left: -75px;
	position: relative;
	-webkit-animation: moveover 15s; /* Chrome, Safari, Opera */
	animation: moveover 15s;
	-webkit-animation-delay: 2s;
	animation-delay: 2s;
	-webkit-animation-direction: alternate;
	animation-direction: alternate;
	-webkit-animation-play-state: running; /* Safari and Chrome */
	animation-play-state: running;
	-webkit-animation-iteration-count: infinite;
	animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
	animation-timing-function: linear;
}
/* Chrome, Safari, Opera */
@-webkit-keyframes moveover {
	0% {
		-webkit-transform: translate3d(-75px, 0px, 0px);
		-webkit-animation-timing-function: linear;
	}
	10% {
		-webkit-transform: translate3d(40px, 5px, 0px);
		-webkit-animation-timing-function: linear;
	}
	20% {
		-webkit-transform: translate3d(300px, 50px, 0px);
		-webkit-animation-timing-function: linear;
	}
	30% {
		-webkit-transform: translate3d(500px, 80px, 0px);
		-webkit-animation-timing-function: ease-out;
	}
	40% {
		-webkit-transform: translate3d(800px, 100px, 0px);
		-webkit-animation-timing-function: ease-in;
	}
	50% {
		-webkit-transform: translate3d(700px, 130px, 0px);
		-webkit-animation-timing-function: linear;
	}
	60% {
		-webkit-transform: translate3d(500px, 40px, 0px);
		-webkit-animation-timing-function: linear;
	}
	70% {
		-webkit-transform: translate3d(300px, 50px, 0px);
		-webkit-animation-timing-function: linear;
	}
	80%  {
		-webkit-transform: translate3d(500px, 120px, 0px);
		-webkit-animation-timing-function: linear;
	}
	90% {
		-webkit-transform: translate3d(800px, 10px, 0px);
		-webkit-animation-timing-function: linear;
	}
	100% {
		-webkit-transform: translate3d(1200px, -40px, 0px);
		-webkit-animation-timing-function: linear;
	}

}
/* Standard syntax */
@keyframes moveover {
	0% {
		transform: translate3d(-75px, 0px, 0px);
		animation-timing-function: linear;
	}
	10% {
		transform: translate3d(40px, 5px, 0px);
		animation-timing-function: linear;
	}
	20% {
		transform: translate3d(300px, 50px, 0px);
		animation-timing-function: linear;
	}
	30% {
		transform: translate3d(500px, 80px, 0px);
		animation-timing-function: ease-out;
	}
	40% {
		transform: translate3d(800px, 100px, 0px);
		animation-timing-function: ease-in;
	}
	50% {
		transform: translate3d(700px, 130px, 0px);
		animation-timing-function: linear;
	}
	60% {
		transform: translate3d(500px, 40px, 0px);
		animation-timing-function: linear;
	}
	70% {
		transform: translate3d(300px, 50px, 0px);
		animation-timing-function: linear;
	}
	80%  {
		transform: translate3d(500px, 120px, 0px);
		animation-timing-function: linear;
	}
	90% {
		transform: translate3d(800px, 10px, 0px);
		animation-timing-function: linear;
	}
	100% {
		transform: translate3d(1200px, -40px, 0px);
		animation-timing-function: linear;
	}
}
</style>
<?php require 'footer-inside.php'; ?>
<?php require 'footer.php'; ?>