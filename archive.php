<?php
/*
Archive
*/
$currentpagename = 'archive';
?>
<?php require 'header.php'; ?>
<?php require 'header-inside.php'; ?>
<div id="contentcontainer">
	<div id="content">
		<?php if (have_posts()) {
			while (have_posts()) {
				the_post(); ?>
				<div class="post">
					<a href="<?php the_permalink() ?>"><h1><?php the_title();?></h1></a>
					<?php the_excerpt(); ?>
				</div>
			<?php }
		} else { ?>
			<strong>Sorry, we couldn't find anything.</strong>
		<?php } ?>
		<div id="postnavpagingnav">
			<?php if(get_next_posts_link()) { ?>
				<div class="nav-previous"><?php next_posts_link('&larr; Older posts'); ?></div>
			<?php } ?>
			<?php if(get_previous_posts_link()) { ?>
				<div class="nav-next"><?php previous_posts_link('Newer posts &rarr;'); ?></div>
			<?php } ?>
		</div>
	</div>
</div>
<?php require 'footer-inside.php'; ?>
<?php require 'footer.php'; ?>