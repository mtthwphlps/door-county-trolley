<?php
/*
Template Name: Woocommerce
*/
$currentpagename = 'woocommerce';
?>
<?php require 'header.php'; ?>
<?php require 'header-inside.php'; ?>
<div id="contentcontainer">
	<div id="content">
		<div id="woocommerceglobalbuttons">
			<a class="button" href="/cart/">View Cart</a>
		</div>
		<?php woocommerce_content(); ?>
	</div>
</div>
<?php require 'footer-inside.php'; ?>
<?php require 'footer.php'; ?>