<?php
// Get current user role
$current_user = wp_get_current_user();
$user_roles = $current_user->roles;
$user_role = array_shift($user_roles);
if ($user_role == 'administrator') {
	$thisuserrole = 'Administrator';
} elseif ($user_role == 'editor') {
	$thisuserrole = 'Editor';
} elseif ($user_role == 'author') {
	$thisuserrole = 'Author';
} elseif ($user_role == 'contributor') {
	$thisuserrole = 'Contributor';
} elseif ($user_role == 'subscriber') {
	$thisuserrole = 'Subscriber';
} elseif ($user_role == 'client') {
	$thisuserrole = 'Client';
} else {
	$thisuserrole = $user_role;
}

// Admin menus
require_once ( get_template_directory() . '/theme-options.php' );

// Login screen customizations
require_once ( get_template_directory() . '/inc/customizethis.php' );

// Menus and widgets
require_once ( get_template_directory() . '/inc/menuswidgets.php' );
require_once ( get_template_directory() . '/inc/admincontrolledmenus.php' );
require_once ( get_template_directory() . '/inc/admincontrolledwidgets.php' );

// Other stuff
require_once ( get_template_directory() . '/inc/foundaframework.php' );
require_once ( get_template_directory() . '/inc/requiredplugins.php' );
require_once ( get_template_directory() . '/inc/widget-classes.php' );

add_theme_support( 'woocommerce' );

function firstrun() {
}
add_action('after_switch_theme', 'firstrun');

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<div><a class="archivemoretag" href="'. get_permalink($post->ID) . '">read more</a></div>';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );
function remove_jquery_migrate( &$scripts) {
	if(!is_admin()) {
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
	}
}
?>