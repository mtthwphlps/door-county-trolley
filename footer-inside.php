			</div>
		</div>
		<div id="inside-footer">
			<div id="inside-footer-top">
			</div>
			<div id="inside-footer-content">
				<div class="inside-footer-row inside-footer-social">
					<?php echo show_social_icons(); ?>
				</div>
				<div class="inside-footer-row inside-footer-copy">
					Door County Trolley * Sturgeon Bay, Wisconsin<br />
					"Specializing In Scenic Tours Of Door County Wisconsin"<br />
					All Rights Reserved
				</div>
				<div class="inside-footer-row inside-footer-credit">
					Site by: <a href="http://foxvalleywebdesign.com" target="_blank">Fox Valley Web Design <sup>TM</sup></a>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	setInterval(function(){
		jQuery("#header-logo-glow").fadeIn(1000);
		setTimeout(function(){
			jQuery("#header-logo-glow").fadeOut(1000);
		}, 1500);
	}, 5000);
});
</script>