<?php
/*
pages
*/
$currentpagename = 'page';
?>
<?php require 'header.php'; ?>
<?php require 'header-inside.php'; ?>
<div id="contentcontainer">
	<div id="content">
		<?php if (have_posts()) {
			while (have_posts()) {
				the_post(); ?>
				<h1><?php the_title();?></h1>
				<?php the_content(); ?>
			<?php }
		} else { ?>
			<strong>Sorry, we couldn't find anything.</strong>
		<?php } ?>
	</div>
</div>
<?php require 'footer-inside.php'; ?>
<?php require 'footer.php'; ?>