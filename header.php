<?php
$foundaheadclass = '';
?>
<!doctype html>
<!-- <?php echo $currentpagename; ?> -->
<!--
Designed by Fox Valley Web Design foxvalleywebdesign.com
Coded by Matthew Phelps
-->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<script src="http://static.mobilewebsiteserver.com/redirect.js" type="text/javascript"></script>
<script type="text/javascript">Mobile_redirect("http://m.mobilewebsiteserver.com/site/doorcountytrolley_1");</script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- Use the .htaccess and remove these lines to avoid edge case issues. More info: h5bp.com/i/378 -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php wp_title( '' ); ?></title>
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/favicon-152.png">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favicon-144.png">
	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/screen.css">
	<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
	<!-- All JavaScript at the bottom, except this Modernizr build. Modernizr enables HTML5 elements & feature detects for optimal performance. Create your own custom Modernizr build: www.modernizr.com/download/ -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/libs/modernizr-2.5.3.min.js"></script>
	<?php if(@$_GET['dev'] == '1') { ?>
		<script src="<?php echo get_template_directory_uri(); ?>/js/live.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/dev.js"></script>
	<?php } ?>
	<!-- Custom fonts here -->
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>
<body class="<?php if(is_front_page()) { echo 'frontpage'; } else { echo 'insidepage'; } ?>">
	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6. chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->