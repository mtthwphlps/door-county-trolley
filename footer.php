<!-- END PAGE FOOTER -->
<!-- JavaScript at the bottom for fast page loading -->
<!-- scripts concatenated and minified via build script -->
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<!--Browser update notice-->
<script type="text/javascript"> 
var $buoop = {} 
$buoop.ol = window.onload; 
window.onload=function(){ 
 try {if ($buoop.ol) $buoop.ol();}catch (e) {} 
 var e = document.createElement("script"); 
 e.setAttribute("type", "text/javascript"); 
 e.setAttribute("src", "http://browser-update.org/update.js"); 
 document.body.appendChild(e); 
} 
</script>
<!--End browser update notice-->
<?php wp_footer(); ?>
<!-- end scripts -->
</body>
</html>